#!/bin/sh
# cv.sh, by ntnsndr, v. 20180729
# Summary: Uses Pandoc to generate CV pdf from source
# Input: None
# Output: A pandoc-proceessed output file

# Command content
pandoc -s -f markdown+smart --variable fontsize=11pt -o ~/Cloud/Dox/Schneider-CV.pdf ~/Cloud/Notes/CV.md
