#!/bin/sh
# pandout, by ntnsndr, v. 20190316
# Summary: Uses Pandoc to generate a multi-format typeset manuscript in output/
# Input: One or more source files in current directory
# Live inputs: Filename, .csl from ~/Zotero/styles/
# Output: A set of pandoc-proceessed output files in the output/ directory
# to specify .csl file for bibliography, use YAML metadata "csl:" command

# Determine filename $fname
fname=""
input="$@"
fname_default="${input%%.*}"
read -p 'Filename [input_filename.*]: ' fname
if [ -z "$fname" ]; then
    fname="$fname_default"
fi

# Command content (see also ~/.emacs)
# remove "--citeproc" for older versions
cmd="pandoc -s -f markdown+smart --citeproc --bibliography $HOME/Zotero/lib.bib  --variable fontsize=11pt -o ./output/$fname"

# Create output/ directory
if [ ! -d 'output' ]; then
    mkdir output
fi

# Run conversion commands for each filetype
# Using full output from commands
$cmd.docx $@
$cmd.epub $@
$cmd.odt $@
$cmd.pdf $@
