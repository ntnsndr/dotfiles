;; elisp script for installing emacs packages via MELPA
(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))
(package-initialize)

;; setting the default directory for extra packages
(add-to-list 'load-path "~/.emacs.d/lisp/")

(package-refresh-contents)
(package-install 'use-package)
(package-install 'magit)
(package-install 'markdown-mode)
(package-install 'pandoc-mode)
(package-install 'wc-mode)
