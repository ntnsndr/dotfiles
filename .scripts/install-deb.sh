#!/bin/bash
# Install script for ntnsndr's Deb environment

echo "Beginning install script for ntnsndr Deb environment"

# Initial utilities
sudo add-apt-repository ppa:nextcloud-devs/client
sudo apt -y update && apt upgrade
sudo apt -y install nextcloud-desktop nautilus-nextcloud
sudo apt -y install keepassxc
sudo apt -y install git
sudo apt -y install xdg-open
sudo apt -y install nodejs
sudo apt -y install flatpak

# emacs
sudo apt -y install emacs-gtk
sudo apt -y install aspell aspell-en
emacs --script ~/.scripts/install-emacs.el

# nvim
sudo apt -y install neovim
~/.scripts/install-vim.sh

# pandoc
sudo apt -y install texlive
sudo apt -y install texlive-latex-extra
sudo apt -y install lmodern
echo "Download pandoc and install from dpkg:"
echo "https://github.com/jgm/pandoc/releases/latest"
read response

# revealjs
cd ~/.revealjs
rm -rf reveal.js/ # remove existing install
git clone https://github.com/hakimel/reveal.js.git
cd reveal.js
npm install
cp ~/.revealjs/ntn.css ~/.revealjs/reveal.js/dist/theme/
cd ~
echo "Check correct versions between reveal and pandoc: https://github.com/jgm/pandoc/wiki/Using-pandoc-to-produce-reveal.js-slides"
read response

# zotero
echo "Restart the computer to finish flatpak installation, then run"
echo "flatpak install flathub org.zotero.Zotero"
echo "Then install"
echo "And be sure that the default data location is ~/Zotero"
echo "Good?"
read response

# fonts
sudo cp ~/.fonts/dinreg.ttf /usr/share/fonts/
echo "Download fonts and put them in /usr/share/fonts/:"
echo "https://github.com/be5invis/Iosevka/releases/latest (home terminal)"
echo "https://www.latofonts.com/lato-free-fonts/ (office system)"
echo "Good?"
read response

echo "Installation script complete"
