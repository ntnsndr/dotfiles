#!/bin/bash
# PANDA, a wrapper for pandoc by ntnsndr

# Check if the required arguments are provided
if [ $# -lt 1 ]; then
 echo "Usage: $0 [required-filename] [output format]"
 exit 1
fi

# Store original directory and move to input file's directory
ORIG_DIR=$(pwd)
cd "$(dirname "$1")"
FILENAME=$(basename "$1")
CURRENT_DIR=$(pwd)

# Set the output format to html if not provided
if [ $# -eq 1 ]; then
 OUTPUT_FORMAT="html"
else
 OUTPUT_FORMAT=$2
fi

# Check if the input file exists
if [ ! -f "$FILENAME" ]; then
 echo "Error: Input file '$FILENAME' does not exist."
 exit 1
fi

# Set the file extension based on the output format
case $OUTPUT_FORMAT in
 pdf)
   FILE_EXT="pdf"
   OUTPUT_FORMAT="latex"
   PANDOC_OPTS="--variable fontsize:12pt"
   ;;
 reveal)
   FILE_EXT="html"
   OUTPUT_FORMAT="revealjs"
   PANDOC_OPTS="--standalone --embed-resources -V revealjs-url=/home/ntnsndr/.revealjs/reveal.js -V theme=ntn -V controls=false --slide-level=2"
   ;;
 pptx)
   FILE_EXT="pptx"
   OUTPUT_FORMAT="pptx"
   PANDOC_OPTS="--reference-doc=/home/ntnsndr/.templates/Slideshow.pptx"
   ;;
 html)
    FILE_EXT="html"
    OUTPUT_FORMAT="html"
    PANDOC_OPTS="--embed-resources --standalone"
    ;;
 *)
   FILE_EXT=$OUTPUT_FORMAT
   PANDOC_OPTS=""
   ;;
esac

# Create temporary files
TMP_IN_FILE="/tmp/pandoc-in.tmp"
TMP_OUT_FILE="/tmp/pandoc-out.$FILE_EXT"

# Check if the temporary files can be created
if [ ! -w /tmp ]; then
 echo "Error: Cannot create temporary files in '/tmp' directory."
 exit 1
fi

# Copy the input file to the temporary file
cp "$FILENAME" "$TMP_IN_FILE"

# Convert the file using pandoc
iconv -t utf-8 "$TMP_IN_FILE" | pandoc -s -f markdown+smart \
   --citeproc --bibliography "$HOME/Zotero/lib.bib" \
   --pdf-engine=xelatex \
   --resource-path="$CURRENT_DIR" \
   -t "$OUTPUT_FORMAT" $PANDOC_OPTS -o "$TMP_OUT_FILE"

# Return to original directory
cd "$ORIG_DIR"

# Check if the pandoc command is successful
if [ $? -ne 0 ]; then
 echo "Error: Pandoc command failed."
 exit 1
fi

# Open the output file
xdg-open "$TMP_OUT_FILE"

# Check if the output file can be opened
if [ $? -ne 0 ]; then
 echo "Error: Cannot open output file '$TMP_OUT_FILE'."
 exit 1
fi
