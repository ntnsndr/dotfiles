#!/bin/bash
# Install script for ntnsndr's Arch environment

echo "Beginning install script for ntnsndr Arch environment"

# First, some basic utilities
sudo pacman -S xdg-utils
sudo pacman -S nextcloud-client
sudo pacman -S keepassxc
sudo pacman -S git
sudo pacman -S nodejs npm

# Ensure .bash_aliases works, not default on Arch
cat > ~/.bashrc << EOF
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
    fi
EOF

# Canon printer
sudo pacman -S cndrvcups-lb
sudo pacman -S system-config-printer
echo "At the end, use the Print Settings GUI to set up the printer"
read response

# emacs
sudo pacman -S emacs
sudo pacman -S aspell-en # English dictionary
emacs --script ~/.scripts/install-emacs.el

# nvim
sudo pacman -S neovim
~/.scripts/install-vim.sh

# pandoc
sudo pacman -S pandoc
sudo pacman -S texlive-core
sudo pacman -S texlive-latexextra

# revealjs
cd ~/.revealjs
rm -rf reveal.js/ # remove existing install
git clone https://github.com/hakimel/reveal.js.git
cd reveal.js
npm install
cp ~/.revealjs/ntn.css ~/.revealjs/reveal.js/dist/theme/
cd ~
echo "Check correct versions between reveal and pandoc: https://github.com/jgm/pandoc/wiki/Using-pandoc-to-produce-reveal.js-slides"
read response

# zotero
sudo pacman -S zotero-bin
echo "For Zotero, see installation notes in ~/Cloud/Notes/Config.md. Good?"
# If we change data location from the default ~/Zotero, update that in .emacs AND pandout.sh
read response

# fonts
sudo cp ~/.fonts/dinreg.ttf /usr/share/fonts/
echo "Download fonts and put them in /usr/share/fonts/:"
echo "https://github.com/be5invis/Iosevka/releases/latest (home terminal)"
echo "https://www.latofonts.com/lato-free-fonts/ (office system)"
echo "Good?"
read response

echo "Installation script complete"
