# Custom aliases for ntnsndr
# Be sure this is referred to in .bashrc

set -o vi

# More or less permanent file aliases:
alias Proximate='nvim ~/Cloud/Notes/Proximate.md'
alias Finances='less ~/Cloud/Current/School/Finances.md'

# More or less permanent directory aliases:
alias Desktop='cd ~/Desktop'
alias Cloud='cd ~/Cloud'
alias Current='cd ~/Cloud/Current'
alias Notes='cd ~/Cloud/Notes'

# More or less permanent app aliases
alias pcloud='~/Apps/pcloud'
alias emacs='emacs -nw'
alias open='xdg-open'
alias e='nvim'
alias q='exit'

# More or less permanent scripts
alias articleupload='~/.scripts/articleupload.sh' # requires one filename as argument
alias cv='~/.scripts/cv.sh'
alias dotupdate='git fetch --all && git reset --hard origin/master'
alias pandout='~/.scripts/pandout.sh'
alias pandorm='rm -rf output'
alias panda='~/.scripts/panda.sh'

# Hopefully temporary utility aliases:
alias paren='xmodmap -e "keycode 81=9 parenleft"'

# More or less permanent utility aliases:
alias editalias='nvim ~/.bash_aliases'
alias rm='rm -i'
alias search='grep -i'
