" ntnsndr's vimrc file

" Basics
syntax on
set noerrorbells
set spell
set notermguicolors

" Margins
set foldcolumn=2
highlight FoldColumn ctermbg=NONE

" Line wrapping in Markdown files 
autocmd FileType markdown setlocal linebreak
autocmd FileType markdown noremap j gj
autocmd FileType markdown noremap k gk

" Tabs
set tabstop=4
set shiftwidth=4
set expandtab

"Search
set ic
set smartcase
set incsearch

" Workspace location
let g:workspace_session_directory = $HOME . '/.vim/sessions/'

" Set shell to bash
set shell=bash\ -l

" Shortcuts
command! Proximate :e ~/Cloud/Notes/Proximate.md

" Create panda command for pandout
command! -nargs=1 Panda execute ':w' | execute ':!~/.scripts/panda.sh % <args>'
