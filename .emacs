;; Emacs init file, by and for ntnsndr

;; INITIALIZE MELPA
;; Keep this on top
;; https://melpa.org/#/getting-started
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; setting the default directory for extra packages
(add-to-list 'load-path "~/.emacs.d/lisp/")

;; SOME BASIC PREFERENCES

;; GUI only
;; Fonts
(add-to-list 'default-frame-alist
             '(font . "Iosevka Extended-16"))

;; Remove startup screen
(setq inhibit-startup-screen t)
;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)
;; turns off bells and whistles
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
;; no bell sounds
(setq ring-bell-function 'ignore)
;; no blinking cursor
(blink-cursor-mode 0)
;; clipboard with ctrl+shift+v
(global-set-key (kbd "C-S-v") 'clipboard-yank)
;; enable word wrap
(global-visual-line-mode t)
;; load word-count tools, use "wc" command to get instant count
(load "wc-mode")
;; 4-space tabs as default
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

;; BOOKMARKS
(setq bookmark-default-file "~/.emacs-bookmarks")

;; MARKDOWN MODE
(autoload 'markdown-mode "markdown-mode" "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;; HORIZONTAL WINDOWS
;; open horizontal windows as default for multiple files
(defun 2-windows-vertical-to-horizontal ()
  (let ((buffers (mapcar 'window-buffer (window-list))))
    (when (= 2 (length buffers))
      (delete-other-windows)
      (set-window-buffer (split-window-horizontally) (cadr buffers)))))
(add-hook 'emacs-startup-hook '2-windows-vertical-to-horizontal)

;; MARGINS
(setq-default left-margin-width 2)
(setq-default right-margin-width 2)

;; SPELLING
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'prog-mode-hook 'flyspell-prog-mode)
;; Turn on Flyspell
(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda ()
                   (flyspell-mode 1)
                   (set-variable 'flyspell-issue-message-flag nil)
                   (flyspell-buffer))))

;; BACKUP CONTROL
;; source: http://is.gd/ag5KOC
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  )

;; FUNCTIONS

;; DICT
;; For getting definitions of words
(defun dict ()
  (interactive)
  (let (word)
    (setq word (read-string "Define: "))
    (shell-command (concat "dict " word))))

;; ASCIIFY
;; replace complex curly quotes with ASCII straight ones
;; replace em-dash with ---
(defun asciify ()
  (interactive)
  (setq start (point))
  (beginning-of-buffer)
  (while (search-forward "“" nil t) (replace-match "\"" nil t))
  (beginning-of-buffer)
  (while (search-forward "”" nil t) (replace-match "\"" nil t))
  (beginning-of-buffer)
  (while (search-forward "‘" nil t) (replace-match "\'" nil t))
  (beginning-of-buffer)
  (while (search-forward "’" nil t) (replace-match "\'" nil t))
  (beginning-of-buffer)
  (while (search-forward "—" nil t) (replace-match "---" nil t))
  (goto-char start))

;; PANDOC
;; for converting markdown text in buffer to another format and opening it
;; file-type argument must be a pandoc-recognized file format
;; to specify .csl file for bibliography, use YAML metadata "csl:" command
(defun pandoc ()
  (interactive)
  (let (file-type file-ext tmp-out-file tmp-out-file out-file current-dir)
    (setq file-type (read-string "File type (docx, epub, [html], odt, pdf, pptx, reveal, etc.): "))
	(setq file-ext file-type)
	;; set default as html
    (when (string-equal file-type "")
	  (setq file-type "html")
	  (setq file-ext "html"))
    ;; set up pdf routine
    (when (string-equal file-type "pdf")
      (setq file-type "latex --variable fontsize='11pt'"))
	;; set up slides revealjs routine
	(when (string-equal file-type "reveal")
	  (setq file-ext "html")
	  ;; Light approach, which requires web access and readable code (but speaker notes and relative paths don't work):
	  ;; (setq file-type "revealjs -V revealjs-url='http://lab.hakim.se/reveal-js' --standalone"))
	  ;; Heavy embedded binary approach (relies on reveal.js/ being in ~/.revealjs/):
      ;; Workaround: For YouTube videos, need to add `data-external="1"` (https://is.gd/UmgYgp)
	  (setq file-type "revealjs --standalone --embed-resources -V revealjs-url='/home/ntnsndr/.revealjs/reveal.js' -V theme=ntn -V controls=false --slide-level=2"))
    ;; set up slides pptx routine
    (when (string-equal file-type "pptx")
      (setq file-ext "pptx")
      (setq file-type "pptx --reference-doc='/home/ntnsndr/.templates/Slideshow.pptx'"))
	;; proceed
    (setq tmp-in-file "/tmp/pandoc-in.tmp")
    (setq tmp-out-file (concat "/tmp/pandoc-out." file-ext))
    (write-region nil nil tmp-in-file)
    (shell-command (concat
                    "iconv -t utf-8 " tmp-in-file " | " ;; pipe in utf-8 for pandoc
                    "pandoc -s" ;; embed full header in HTML
                    " -f markdown+smart" ;; add curly quotes
                    " --citeproc --bibliography $HOME/Zotero/lib.bib" ;; connect to BibTeX export from Zotero
                    ;; ^ remove "--citeproc" for older versions
                    " --pdf-engine=xelatex" ;; for special characters
                    " -t " file-type
                    " -o " tmp-out-file))
;; End convert routine
    (shell-command (concat "xdg-open " tmp-out-file))
   )
)

;; RANDOM AUTO STUFF
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#d2ceda" "#f2241f" "#67b11d" "#b1951d" "#3a81c3" "#a31db1" "#21b8c7" "#655370"])
 '(custom-safe-themes
   '("7fd8b914e340283c189980cd1883dbdef67080ad1a3a9cc3df864ca53bdc89cf" "bbb13492a15c3258f29c21d251da1e62f1abb8bbd492386a673dcfab474186af" default))
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#3a81c3")
     ("OKAY" . "#3a81c3")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#42ae2c")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f")))
 '(ispell-dictionary nil)
 '(org-fontify-done-headline nil)
 '(org-fontify-todo-headline nil)
 '(package-selected-packages
   '(magit wc-mode spacemacs-theme pandoc-mode markdown-mode gptel ellama))
 '(pdf-view-midnight-colors '("#655370" . "#fbf8ef")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
