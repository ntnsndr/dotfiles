# Dotfiles

[https://gitlab.com/ntnsndr/dotfiles/](https://gitlab.com/ntnsndr/dotfiles/)

This is a set of tools to facilitate rapid switching among Linux-based operating systems.

## Initial install

Load the dotfiles into the home directory:

    cd ~
    git clone https://gitlab.com/ntnsndr/dotfiles.git
    mv ~/dotfiles/{.,}* ~/

Then execute the appropriate script. On Arch-based systems:

    chmod +x ~/.scripts/install-arch.sh
    ~/.scripts/install-arch.sh 

On Debian-based systems:

    chmod +x ~/.scripts/install-deb.sh
    ~/.scripts/install-deb.sh

## Updating

Update locally or on GitLab. Pull updates from this repo and push changes back.
